﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Target : MonoBehaviour
{
	[SerializeField]
	private	Circular_Gauge cGauge;

	void Awake()
	{
		//cGauge = GameObject.Find("Circular_Gauge").GetComponent<Circular_Gauge>();
		
		/*EventTriggerManager.AddEventTriggerListener(GetComponent<EventTrigger>(),
			EventTriggerType.PointerEnter, cGauge.Enter_Gauge);

		EventTriggerManager.AddEventTriggerListener(GetComponent<EventTrigger>(),
			EventTriggerType.PointerExit, cGauge.Exit_Gauge);*/
	}
	void Update()
	{
		

#if UNITY_EDITOR
        float x = Input.GetAxis("Mouse X");
		float y = Input.GetAxis("Mouse Y");

#else
        float x = Input.GetAxis("Horizontal");
		float y = Input.GetAxis("Vertical");
#endif

        transform.position += new Vector3(x, y, .0f) * 5.0f * Time.deltaTime;

		if ( cGauge.Is_Full )
		{
			SetColor();
			cGauge.Exit_Gauge();
		}
	}
	public void SetColor()
	{
		transform.GetComponent<Renderer>().material.color = new Color(Random.Range(.0f, 1.0f),
			Random.Range(.0f, 1.0f), Random.Range(.0f, 1.0f), 1.0f);
	}
}

