﻿using UnityEngine;
using System.Collections;

public class MouseRotate : MonoBehaviour {
    float yaw;
    float pitch;

	
	// Update is called once per frame
	void Update () {

        yaw = Mathf.Clamp(yaw = Input.GetAxis("Mouse X"), -30f, 30f);
        pitch = Mathf.Clamp(pitch = Input.GetAxis("Mouse Y"), -30f, 30f);


        transform.rotation = Quaternion.Euler(new Vector3(pitch, -yaw, 0f));

    }
}
