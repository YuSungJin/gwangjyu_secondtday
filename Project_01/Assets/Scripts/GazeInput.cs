﻿using UnityEngine;
using System.Collections;

public class GazeInput : MonoBehaviour {

	GameObject curStay = null;

	void Start()
	{
	}

	void Update()
	{
		RaycastHit hitInfo;
		if (Physics.Raycast (Camera.main.transform.position, Camera.main.transform.forward, out hitInfo, 9999f)) {

//			Debug.Log (hitInfo.collider.name);

			if (curStay == null) {
				hitInfo.collider.SendMessage ("OnGazeEnter", SendMessageOptions.DontRequireReceiver);
				curStay = hitInfo.collider.gameObject;
				SendMessage ("OnHitTarget", hitInfo, SendMessageOptions.DontRequireReceiver);
			}
		} else {

			if (curStay != null) {
				curStay.SendMessage ("OnGazeExit", SendMessageOptions.DontRequireReceiver);
				curStay = null;
			}
		}
	}

//	void OnDrawGizmos()
//	{
//		Gizmos.DrawLine (transform.forward, transform.forward * 999f);
//	}
}
