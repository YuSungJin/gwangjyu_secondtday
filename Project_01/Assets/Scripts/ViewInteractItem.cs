﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ViewInteractItem : MonoBehaviour{
    public string NormalTrigger = "Normal";
    public string SelectedTrigger = "Selected";


    List<Collider> colliders;

    public List<Collider> Colliders
    {
        get{
            if (colliders == null)
                colliders = new List<Collider>();
            return colliders;
        }
        
    }

    void OnTriggerEnter(Collider col)
    {

        Colliders.Add(col);
        GetComponent<Animator>().SetTrigger(SelectedTrigger);

    }

    void OnTriggerExit(Collider col)
    {

        Colliders.Remove(col);
        if (Colliders.Count == 0)
            GetComponent<Animator>().SetTrigger(NormalTrigger);

    }

}
